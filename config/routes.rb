Rails.application.routes.draw do
  root   'static_pages#about'
  get    'signup'    => 'users#new'
  get    'login'     => 'sessions#new'
  post   'login'     => 'sessions#create'
  delete 'logout'    => 'sessions#destroy'

  get 'about' => 'static_pages#about'
  get 'leadership' => 'static_pages#leadership'
  get 'competences' => 'static_pages#competences'
  get 'statistics' => 'static_pages#statistics'
  get 'projects' => 'static_pages#projects'
  get 'companies' => 'static_pages#companies'
  get 'eae' => 'static_pages#eae'
  get 'sezam' => 'static_pages#sezam'
  get 'pem' => 'static_pages#pem'
  get 'gem' => 'static_pages#gem'
  get 'promcenter' => 'static_pages#promcenter'

  resources :users, only: [:show, :index, :create, :edit, :update, :destroy] do
    get 'topics' => 'users#topics'
    get 'messages' => 'users#messages'
  end

  resources :forums, only: [:index, :show, :edit, :create, :update, :destroy] do
      resources :topics, only: [:show, :new, :create, :edit, :update, :destroy]
  end

  resources :messages, only: [:create, :edit, :update, :destroy]

  resources :password_resets, only: [:new, :create, :edit, :update]

  resources :account_activations, only: [:edit]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
