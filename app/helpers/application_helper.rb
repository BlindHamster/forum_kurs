module ApplicationHelper
  def full_title (page_title = '')
    base_title = "\"ЭНЕРГОАТОМИНЖИНИРИНГ\""
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def find_page(per_page, index)
    if (index <= per_page)
      return "1"
    elsif (index % per_page == 0)
      page = index/per_page
      return page.to_s
    else
      page = index/per_page + 1
      return page.to_s
    end
  end

  def name_with_restriction object
    if object.restricted == "mod"
      name = "(M)#{object.name}"
    elsif object.restricted == "employee"
      name = "(C)#{object.name}"
    else
      name = object.name
    end
    name
  end


end
