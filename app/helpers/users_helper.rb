module UsersHelper

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Пожалуйста, войдите на форум!"
      redirect_to login_url
    end
  end

  def correct_user
    @user = User.find(params[:id])
    unless ((@user == current_user) || @current_user.admin? || @current_user.mod?)
        flash[:danger] = "У вас нет прав доступа к этой странице!"
        redirect_back fallback_location: forums_url
    end
  end

  def administrator
    unless current_user.admin?
        flash[:danger] = "У вас нет прав доступа для этого действия!"
        redirect_back fallback_location: forums_url
    end
  end

end
