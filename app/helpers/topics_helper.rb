module TopicsHelper
  def correct_user_t
    @user = User.find(Topic.find(params[:id]).user_id)
    unless ((@user == current_user) || @current_user.admin? || @current_user.mod?)
        flash[:danger] = "У вас нет прав доступа к этой страницы!"
        redirect_back fallback_location: forums_url
    end
  end
end
