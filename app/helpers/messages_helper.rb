module MessagesHelper
  def correct_user_m
    @message = Message.find(params[:id])
    @user = User.find(@message.user_id)
    unless ((@user == current_user) || @current_user.admin? || @current_user.mod?)
        flash[:danger] = "У вас нет достаточных прав доступа!"
        redirect_back fallback_location: forums_url
    end
  end
end
