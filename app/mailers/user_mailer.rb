class UserMailer < ActionMailer::Base

default from: "ЭНЕРГОАТОМИНЖИНИРИНГ"
default 'Content-Transfer-Encoding' => '7bit'

def password_reset (user)
  @user = user
  mail to: user.email, subject: "Сброс пароля"
end

def account_activation (user)
  @user = user
  mail to: user.email, subject: "Активация аккаунта"
end

end
