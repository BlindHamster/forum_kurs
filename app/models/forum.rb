class Forum < ApplicationRecord
  belongs_to :user
  has_many :topics, :dependent => :destroy

  validates :name, presence: { message: "Имя раздела не может быть пустым" },
  length: { in: 5..40, message: "В имени раздела должно быть от 5 до 40 символов"},
  uniqueness: {case_sensitive: false, message: "Такое имя раздела уже существует"}
  validates :user_id, presence: { message: "Форум должен быть принадлежать пользователю" }

  def count_messages
    topics = self.topics.all
    n = 0
    topics.each do |t|
      n += t.messages.count
    end
    return n
  end

end
