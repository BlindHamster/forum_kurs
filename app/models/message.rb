class Message < ApplicationRecord
  belongs_to :user
  belongs_to :topic

  validates :content, presence: { message: "Сообщение не может быть пустым" },
  length: { in: 1..5000, message: "В сообщении должно быть от 1 до 5000 символов"}
  validates :user_id, presence: { message: "Сообщение должно принадлежать пользователю" }
  validates :topic_id, presence: { message: "Сообщение должно принадлежать теме" }
end
