class User < ApplicationRecord
  attr_accessor :reset_token, :remember_token, :activation_token
  has_many :forums, :dependent => :destroy
  has_many :topics, :dependent => :destroy
  has_many :messages, :dependent => :destroy
  before_save {self.email.downcase!}
  before_create :create_activation_digest

  VALID_NAME_REGEX = /\A[a-zA-Z\d_]+\z/
  validates :name, presence: { message: "Имя не может быть пустым" },
  length: { in: 3..20, message: "В имени должно быть от 3 до 20 символов"},
  format: { with: VALID_NAME_REGEX, message: "В имени могут использоваться латинские буквы, цифры и нижние подчёркивания" },
  uniqueness: {case_sensitive: false, message: "Такое имя уже существует"}

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: { message: "Почта не может быть пустой" },
  length: { in: 5..100, message: "В почте должно быть от 5 до 100 символов" },
  format: { with: VALID_EMAIL_REGEX, message: "Почта введена некорректно" },
  uniqueness: { case_sensitive: false, message: "Такая почта уже существует"}

  has_secure_password
  validates :password, length: { minimum: 8, message: "В пароле должно быть не менее восьми символов" }, allow_blank: true

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest, User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def password_reset_expired?
    reset_sent_at < 1.hours.ago
  end

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def create_remember_digest
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def forget_remember_digest
    update_attribute(:remember_digest, nil)
  end

  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end

  def activate
    update_attribute(:activated, true)
    update_attribute(:activated_at, Time.zone.now)
  end

  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  def admin?
    if self.role == "admin"
      return true
    else
      return false
    end
  end

  def mod?
    if self.role == "mod"
      return true
    else
      return false
    end
  end

  def employee?
    if self.role == "employee"
      return true
    else
      return false
    end
  end

end
