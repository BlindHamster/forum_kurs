class Topic < ApplicationRecord
  belongs_to :user
  belongs_to :forum
  has_many :messages, :dependent => :destroy

  validates :name, presence: { message: "Имя темы не может быть пустым" },
  length: { in: 5..40, message: "В имени темы должно быть от 5 до 40 символов"},
  uniqueness: {case_sensitive: false, message: "Такое имя темы уже существует"}
  validates :content, presence: { message: "Тема не может быть пустой" },
  length: { in: 1..5000, message: "В теме должно быть от 1 до 5000 символов"}
  validates :user_id, presence: { message: "Тема должна принадлежать пользователю" }
  validates :forum_id, presence: { message: "Тема должна принадлежать форуму" }
end
