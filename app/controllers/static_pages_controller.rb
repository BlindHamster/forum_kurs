class StaticPagesController < ApplicationController

  def about
  end

  def leadership
  end

  def competences
  end

  def statistics
  end

  def projects
  end

  def companies
  end

  def eae
  end

  def sezam
  end

  def pem
  end

  def gem
  end

  def promcenter
  end
end
