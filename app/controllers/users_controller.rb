class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy,
                                        :topics, :messages]
  before_action :correct_user, only: [:edit, :update]
  before_action :administrator, only: :destroy


  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "На вашу почту выслано письмо с ссылкой для активации аккаунта."
      redirect_to forums_url
    else
      render 'new'
    end
  end

  def index
    @users = User.paginate(:page => params[:page], :per_page => 30)
  end

  def edit
    @user = User.find(params[:id])
  end

  def topics
    @user = User.find(params[:user_id])
    @topics = extract_topics(@user).paginate(:page => params[:page], :per_page => 10)
  end

  def messages
    @user = User.find(params[:user_id])
    @messages = extract_messages(@user).paginate(:page => params[:page], :per_page => 10)
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(update_params)
      flash[:success] = "Профиль успешно обновлён!"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    redirect_to users_url
  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def update_params
    upd = params.require(:user).permit(:name, :email, :about, :role, :password, :password_confirmation)
    upd[:redacted_by] = current_user.id
    upd
  end

  def extract_topics user
    if current_user != nil && (@current_user.admin? || @current_user.mod?)
      topics = @user.topics.all
    elsif @current_user != nil && @current_user.employee?
      topics = @user.topics.where(restricted: ["employee", "user"])
    else
      topics = @user.topics.where(restricted: "user")
    end
    topics
  end

  def extract_messages user
    if current_user != nil && (@current_user.admin? || @current_user.mod?)
      messages = @user.messages.all
    elsif @current_user != nil && @current_user.employee?
      messages = @user.messages.where(restricted: ["employee", "user"])
    else
      messages = @user.messages.where(restricted: "user")
    end
    messages
  end

end
