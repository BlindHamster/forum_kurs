class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
    @user = User.find_by(email: params[:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = "Письмо с инструкциями отправлено."
      redirect_to forums_url
    else
      flash.now[:danger] = "Неверная электронная почта!"
      render 'new'
    end
  end

  def edit
  end

  def update
    if password_blank?
      flash.now[:danger] = "Пароль не может быть пустым!"
      render 'edit'
    elsif @user.update_attributes(user_params)
      login @user
      flash[:success] = "Пароль был успешно изменен!"
      redirect_to @user
    else
      flash.now[:danger] = "Некорректный пароль!"
      render 'edit'
    end
  end

  private

  def user_params
    params.permit(:password, :password_confirmation)
  end

  def password_blank?
    params[:password].empty?
  end

  def get_user
    @user = User.find_by(email: params[:email])
    if @user == nil
      flash[:danger] = "Пользователя с такой электронной почтой не существует."
    end
  end

  def valid_user
    unless (@user && @user.authenticated?(:reset, params[:id]))
      flash[:danger] = "Ссылка сброса пароля недействительна."
      redirect_to new_password_reset_url
    end
  end

  def check_expiration
    if @user.password_reset_expired?
      flash[:danger] = "У ссылки вышел срок годности. Поробуйте ещё раз."
      redirect_to new_password_reset_url
    end
  end

end
