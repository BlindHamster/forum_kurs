class MessagesController < ApplicationController
  before_action :logged_in_user, only: [:create, :edit, :update, :destroy]
  before_action :correct_user_m, only: [:edit, :update, :destroy]

  def create
    @message = current_user.messages.new(message_params)
    flash[:danger] = "Сообщение должно содержать от 1 до 5000 символов!" unless @message.save
    redirect_to forum_topic_url(params[:forum_id], params[:id])
  end

  def destroy
      @message = Message.find(params[:id]).destroy
      redirect_back fallback_location: forum_topic_url(Topic.find(@message.topic_id).forum_id, @message.topic_id)
  end

  def edit
    @message = Message.find(params[:id])
  end

  def update
    @message = Message.find(params[:id])
    if @message.update_attributes(update_params)
      flash[:info] = "Сообщение успешно изменено!"
      redirect_to forum_topic_url(Topic.find(@message.topic_id).forum_id, @message.topic_id)
    else
      render 'edit'
    end
  end

  private

  def message_params
    m_params = params.require(:message).permit(:content)
    m_params[:topic_id] = params[:id]
    t = Topic.find(params[:id]).restricted
    if t == "mod"
      m_params[:restricted] = :mod
    elsif t == "employee"
      m_params[:restricted] = :employee
    end
    m_params
  end

  def update_params
    m_params = params.require(:message).permit(:content, :redacted_by, :restricted)
    m_params[:redacted_by] = current_user.id
    m_params
  end

end
