class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user && user.authenticate(params[:password])
      if user.activated?
        login(user)
        params[:remember_me] == '1' ? remember(user) : forget(user)
        redirect_to forums_url
      else
        flash[:danger] = "Аккаунт не активирован. Проверьте свою почту, чтобы найти письмо с ссылкой активации!"
        user.send_activation_email
        redirect_to forums_url
      end
    else
      flash.now[:danger] = 'Неверный пароль или электронная почта!'
      render 'new'
    end
  end

  def destroy
    logout if logged_in?
    redirect_to forums_url
  end

end
