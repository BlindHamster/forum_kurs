class AccountActivationsController < ApplicationController

  def edit
    user = User.find_by(email: params[:email])
    if user && user.authenticated?(:activation, params[:id])
      user.activate
      login user
      flash[:success] = "Аккаунт успешно активирован!"
      redirect_to user
    else
      flas[:danger] = "Неверная ссылка активации!"
      redirect_to forums_url
    end
  end

end
