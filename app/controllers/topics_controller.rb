class TopicsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :correct_user_t, only: [:edit, :update, :destroy]

    def show
      @topic = Topic.find(params[:id])
      @messages = @topic.messages.all.paginate(:page => params[:page], :per_page => 20)
      if logged_in?
        @message = @topic.messages.new
      end
    end

    def new
      @topic = current_user.topics.new
    end

    def create
      @topic = current_user.topics.new(topic_params)
      if @topic.save
        flash[:success] = "Новая тема успешно создана!"
        redirect_to forum_url(params[:forum_id])
      else
        render 'new'
      end
    end

    def destroy
      Topic.find(params[:id]).destroy
      redirect_back fallback_location: forum_url(params[:forum_id])
    end

    def edit
      @topic = Topic.find(params[:id])
    end

    def update
      @topic = Topic.find(params[:id])

      if @topic.restricted != params[:topic][:restricted]
        @topic.messages.all.each do |m|
          m.update_attributes(update_params_m)
        end
      end

      if @topic.update_attributes(update_params)
        flash[:success] = "Тема успешно обновлена!"
        redirect_to forum_topic_url(@topic.forum_id, @topic)
      else
        render 'edit'
      end

    end

    private

    def topic_params
      t_params = params.require(:topic).permit(:name, :content)
      t_params[:forum_id] = params[:forum_id]
      f = Forum.find(params[:id]).restricted
      if f == "mod"
        t_params[:restricted] = :mod
      elsif t == "employee"
        t_params[:restricted] = :employee
      end
      t_params
    end

    def update_params
      t_params = params.require(:topic).permit(:name, :content, :restricted)
      t_params[:redacted_by] = current_user.id
      t_params
    end

    def update_params_m
      params.require(:topic).permit(:restricted)
    end
end
