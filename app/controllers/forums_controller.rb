class ForumsController < ApplicationController
  before_action :logged_in_user, only: [:create, :edit, :update, :destroy]
  before_action :administrator, only: [:create, :edit, :update, :destroy]

  def create
    @forum = current_user.forums.new(forum_params)
    if @forum.save
      flash[:success] = "Новый раздел создан успешно!"
      redirect_to forums_url
    else
      @forums = extract_forums
      flash.now[:danger] = "Некорректное имя раздела!"
      render 'index'
    end
  end

  def edit
    @forum = Forum.find(params[:id])
  end

  def index
    @forums = extract_forums
    if logged_in? && (current_user.admin? || current_user.mod?)
      @forum = current_user.forums.new
    end
  end

  def destroy
    Forum.find(params[:id]).destroy
    redirect_to forums_url
  end

  def show
    @forum = Forum.find(params[:id])
    @topics = extract_topics(@forum)

    @topics = @topics.paginate(:page => params[:page], :per_page => 15)

    if logged_in? && (current_user.admin? || current_user.mod?)
      @topic = @forum.topics.new
    end
  end

  def update
    @forum = Forum.find(params[:id])
    if @forum.update_attributes(forum_params)
      flash[:success] = "Раздел успешно изменен!"
      redirect_to @forum
    else
      render 'edit'
    end
  end

  private
  def forum_params
    params.require(:forum).permit(:name, :restricted)
  end

  def extract_forums
    if current_user != nil && (@current_user.admin? || @current_user.mod?)
      forums = Forum.all
    elsif @current_user != nil && @current_user.employee?
      forums = Forum.where(restricted: ["employee", "user"])
    else
      forums = Forum.where(restricted: "user")
    end
    forums
  end

  def extract_topics forum
    if current_user != nil && (@current_user.admin? || @current_user.mod?)
      topics = forum.topics.all
    elsif @current_user != nil && @current_user.employee?
      topics = forum.topics.where(restricted: ["employee", "user"])
    else
      topics = forum.topics.where(restricted: "user")
    end
    topics
  end

end
