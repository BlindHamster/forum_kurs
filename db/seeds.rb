# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name: "Admin",
             email: "admin@mail.com",
             role: :admin,
             activated: true,
             activated_at: Time.zone.now,
             password: "password",
             password_confirmation: "password")

User.create!(name: "Moderator",
             email: "moderator@mail.com",
             role: :mod,
             activated: true,
             activated_at: Time.zone.now,
             password: "password",
             password_confirmation: "password")

User.create!(name: "Employee",
              email: "employee@mail.com",
              role: :employee,
              activated: true,
              activated_at: Time.zone.now,
              password: "password",
              password_confirmation: "password")

50.times do |n|
  name = Faker::Name.first_name + "_#{n}"
  email = "#{name}@mail.com"
  password = "password"
  User.create!(name: name,
               email: email,
               role: :user,
               activated: true,
               activated_at: Time.zone.now,
               password: password,
               password_confirmation: password)
end

2.times do |n|
  User.find_by(name: "Admin").forums.create!(name: "Раздел##{n}")
end

1.times do |n|
  User.find_by(name: "Admin").forums.create!(name: "Раздел##{n+2}", restricted: :mod)
end

1.times do |n|
  User.find_by(name: "Admin").forums.create!(name: "Раздел##{n+3}", restricted: :employee)
end

Forum.all.each do |f|
  if f.restricted == "mod"
    25.times do |n|
      user_id = Faker::Number.between(1, 2)
      name = "Lorem_" + Faker::Lorem.word + "_#{f.id}" + "_#{n}"
      content = Faker::Lorem.paragraphs(6).join(" ")
      f.topics.create!(name: name, content: content, user_id: user_id, restricted: :mod)
    end
  elsif f.restricted == "employee"
    25.times do |n|
      user_id = Faker::Number.between(1, 3)
      name = "Lorem_" + Faker::Lorem.word + "_#{f.id}" + "_#{n}"
      content = Faker::Lorem.paragraphs(6).join(" ")
      f.topics.create!(name: name, content: content, user_id: user_id, restricted: :employee)
    end
  else
    25.times do |n|
      user_id = Faker::Number.between(1, 53)
      name = "Lorem_" + Faker::Lorem.word + "_#{f.id}" + "_#{n}"
      content = Faker::Lorem.paragraphs(6).join(" ")
      f.topics.create!(name: name, content: content, user_id: user_id)
    end
  end
end

Topic.all.each do |t|
  if t.restricted == "mod"
    30.times do |m|
      user_id = Faker::Number.between(1, 2)
      content = Faker::StarWars.quote
      t.messages.create!(content: content, restricted: :mod, user_id: user_id)
    end
  elsif t.restricted == "employee"
    30.times do |m|
      user_id = Faker::Number.between(1, 3)
      content = Faker::StarWars.quote
      t.messages.create!(content: content, restricted: :employee, user_id: user_id)
    end
  else
    30.times do |m|
      user_id = Faker::Number.between(1, 53)
      content = Faker::StarWars.quote
      t.messages.create!(content: content, user_id: user_id)
    end
  end
end
