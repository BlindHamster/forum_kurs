class CreateForums < ActiveRecord::Migration[5.0]
  def change
    create_table :forums do |t|
      t.string :name
      t.string :restricted, default: :user
      t.integer :redacted_by
      t.references :user, index: true
      t.timestamps null: false
    end
  add_foreign_key :forums, :users
  add_index :forums, :name, unique: true
  end
end
