class CreateTopics < ActiveRecord::Migration[5.0]
  def change
    create_table :topics do |t|
      t.string :name
      t.text :content
      t.string :restricted, default: :user
      t.integer :redacted_by
      t.references :user, index: true
      t.references :forum, index: true
      t.timestamps null: false
    end
    add_foreign_key :topics, :users
    add_foreign_key :topics, :forums
    add_index :topics, :name, unique: true
  end
end
