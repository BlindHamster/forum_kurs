class CreateMessages < ActiveRecord::Migration[5.0]
    def change
      create_table :messages do |t|
        t.text :content
        t.integer :redacted_by
        t.string :restricted, default: :user
        t.references :user, index: true
        t.references :topic, index: true
        t.timestamps null: false
      end
      add_foreign_key :messages, :users
      add_foreign_key :messages, :topics
      add_index :messages, [:user_id, :created_at], unique: true
    end
  end
